# Elide Spring Boot Example

An archetype Elide project using Spring Boot.

## Background

This project is the sample code for [Elide's Getting Started documentation](https://elide.io/pages/guide/01-start.html).

## Install

To build and run:

1. mvn clean install
2. java -jar target/elide-spring-boot-1.0.jar
3. Browse http://localhost:8080/

To run from Heroku:

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/yahoo/elide-spring-boot-example)

## Usage

See [Elide's Getting Started documentation](https://elide.io/pages/guide/01-start.html).

## Example postman calls

**The standard root element, all relationships inline**  
localhost:8080/api/v1/master

**Root object with pagination**  
localhost:8080/api/v1/master?page[number]=1&page[size]=1  

**The root with the selected relationships included inline**    
localhost:8080/api/v1/master?include=tags,masterDetails  

**No other relationships, only the selected fields from Master**    
localhost:8080/api/v1/master?fields[master]=mstFirstname

## License
This project is licensed under the terms of the [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html) open source license.
Please refer to [LICENSE](LICENSE.txt) for the full terms.
