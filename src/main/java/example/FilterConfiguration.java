/*
 * Copyright 2019, Verizon Media.
 * Licensed under the Apache License, Version 2.0
 * See LICENSE file in project root for terms.
 */

package example;

import java.util.HashMap;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.*;

import com.yahoo.elide.core.EntityDictionary;
import com.yahoo.elide.security.checks.Check;

import ch.qos.logback.access.servlet.TeeFilter;
import example.models.filterchecks.FilterProfiles;

@Configuration
public class FilterConfiguration {

  @Bean
  public FilterRegistrationBean requestResponseFilter() {
    final FilterRegistrationBean<TeeFilter> filterRegBean = new FilterRegistrationBean<>();
    TeeFilter filter = new TeeFilter();
    filterRegBean.setFilter(filter);
    filterRegBean.addUrlPatterns("/*");
    filterRegBean.setName("Elide Request Response Filter");
    filterRegBean.setAsyncSupported(Boolean.TRUE);        
    return filterRegBean;
  }


  /**
   * Override the Entity Dictionary to add some checks and life cycle hooks.
   * 
   * By default, auto configuration creates an EntityDictionary with no checks or 
   * life cycle hooks registered. It does register spring as the dependency injection framework for Elide model injection.
   * 
   * @param beanFactory
   * @return
   */
  @Bean
  public EntityDictionary buildDictionary(AutowireCapableBeanFactory beanFactory) {
    
    // List of available checks (User Checks / Filter Checks / etc)
    // Important! The text MUST match up to the text used in the Filter itself.
    HashMap<String, Class<? extends Check>> checkMappings = new HashMap<>();
    checkMappings.put(FilterProfiles.CHECK_SYSNAME, FilterProfiles.class);

    EntityDictionary dictionary = new EntityDictionary(checkMappings, beanFactory::autowireBean);
    dictionary.scanForSecurityChecks(); // Looks for any classes with the @SecurityCheck annotation and auto applies them.

    return dictionary;
  }

}
