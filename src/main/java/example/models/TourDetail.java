package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * TourDetail generated by hbm2java
 */
@Entity
@Table(name = "TOUR_DETAIL")
public class TourDetail implements java.io.Serializable {

  private int tourDetailId;
  private Terminal terminal;
  private Tour tour;
  private int tdOrdinal;
  private int tdSecondsAllowed;

  public TourDetail() {}

  public TourDetail(int tourDetailId, Terminal terminal, Tour tour, int tdOrdinal, int tdSecondsAllowed) {
    this.tourDetailId = tourDetailId;
    this.terminal = terminal;
    this.tour = tour;
    this.tdOrdinal = tdOrdinal;
    this.tdSecondsAllowed = tdSecondsAllowed;
  }

  @Id

  @Column(name = "TOUR_DETAIL_ID", unique = true, nullable = false)
  public int getTourDetailId() {
    return this.tourDetailId;
  }

  public void setTourDetailId(int tourDetailId) {
    this.tourDetailId = tourDetailId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TERMINAL_ID", nullable = false)
  public Terminal getTerminal() {
    return this.terminal;
  }

  public void setTerminal(Terminal terminal) {
    this.terminal = terminal;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TOUR_ID", nullable = false)
  public Tour getTour() {
    return this.tour;
  }

  public void setTour(Tour tour) {
    this.tour = tour;
  }

  @Column(name = "TD_ORDINAL", nullable = false)
  public int getTdOrdinal() {
    return this.tdOrdinal;
  }

  public void setTdOrdinal(int tdOrdinal) {
    this.tdOrdinal = tdOrdinal;
  }

  @Column(name = "TD_SECONDS_ALLOWED", nullable = false)
  public int getTdSecondsAllowed() {
    return this.tdSecondsAllowed;
  }

  public void setTdSecondsAllowed(int tdSecondsAllowed) {
    this.tdSecondsAllowed = tdSecondsAllowed;
  }

}
