package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * AuditActionType generated by hbm2java
 */
@Entity
@Table(name = "AUDIT_ACTION_TYPE", uniqueConstraints = @UniqueConstraint(columnNames = "AAT_SYSNAME"))
public class AuditActionType implements java.io.Serializable {

  private int auditActionTypeId;
  private String aatSysname;
  private String aatDisplayName;
  private Set<AuditDetail> auditDetails = new HashSet<AuditDetail>(0);
  private Set<SettingData> settingDatas = new HashSet<SettingData>(0);

  public AuditActionType() {}

  public AuditActionType(int auditActionTypeId, String aatSysname, String aatDisplayName) {
    this.auditActionTypeId = auditActionTypeId;
    this.aatSysname = aatSysname;
    this.aatDisplayName = aatDisplayName;
  }

  public AuditActionType(int auditActionTypeId, String aatSysname, String aatDisplayName, Set<AuditDetail> auditDetails, Set<SettingData> settingDatas) {
    this.auditActionTypeId = auditActionTypeId;
    this.aatSysname = aatSysname;
    this.aatDisplayName = aatDisplayName;
    this.auditDetails = auditDetails;
    this.settingDatas = settingDatas;
  }

  @Id

  @Column(name = "AUDIT_ACTION_TYPE_ID", unique = true, nullable = false)
  public int getAuditActionTypeId() {
    return this.auditActionTypeId;
  }

  public void setAuditActionTypeId(int auditActionTypeId) {
    this.auditActionTypeId = auditActionTypeId;
  }

  @Column(name = "AAT_SYSNAME", unique = true, nullable = false, length = 64)
  public String getAatSysname() {
    return this.aatSysname;
  }

  public void setAatSysname(String aatSysname) {
    this.aatSysname = aatSysname;
  }

  @Column(name = "AAT_DISPLAY_NAME", nullable = false, length = 64)
  public String getAatDisplayName() {
    return this.aatDisplayName;
  }

  public void setAatDisplayName(String aatDisplayName) {
    this.aatDisplayName = aatDisplayName;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "auditActionType")
  public Set<AuditDetail> getAuditDetails() {
    return this.auditDetails;
  }

  public void setAuditDetails(Set<AuditDetail> auditDetails) {
    this.auditDetails = auditDetails;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "auditActionType")
  public Set<SettingData> getSettingDatas() {
    return this.settingDatas;
  }

  public void setSettingDatas(Set<SettingData> settingDatas) {
    this.settingDatas = settingDatas;
  }

}
