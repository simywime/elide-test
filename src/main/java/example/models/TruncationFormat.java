package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TruncationFormat generated by hbm2java
 */
@Entity
@Table(name = "TRUNCATION_FORMAT")
public class TruncationFormat implements java.io.Serializable {

  private int truncationFormatId;
  private String tfNameOriginal;
  private String tfTagMask;
  private String tfSiteMask;
  private int tfDropZero;
  private int tfEditable;
  private int tfPacsBits;
  private String tfFormat;
  private String tfNameConcat;
  private Set<TagType> tagTypes = new HashSet<TagType>(0);
  private Set<Translation> translations = new HashSet<Translation>(0);

  public TruncationFormat() {}

  public TruncationFormat(int truncationFormatId, String tfNameOriginal, String tfTagMask, String tfSiteMask, int tfDropZero, int tfEditable, int tfPacsBits) {
    this.truncationFormatId = truncationFormatId;
    this.tfNameOriginal = tfNameOriginal;
    this.tfTagMask = tfTagMask;
    this.tfSiteMask = tfSiteMask;
    this.tfDropZero = tfDropZero;
    this.tfEditable = tfEditable;
    this.tfPacsBits = tfPacsBits;
  }

  public TruncationFormat(int truncationFormatId, String tfNameOriginal, String tfTagMask, String tfSiteMask, int tfDropZero, int tfEditable, int tfPacsBits, String tfFormat, String tfNameConcat, Set<TagType> tagTypes, Set<Translation> translations) {
    this.truncationFormatId = truncationFormatId;
    this.tfNameOriginal = tfNameOriginal;
    this.tfTagMask = tfTagMask;
    this.tfSiteMask = tfSiteMask;
    this.tfDropZero = tfDropZero;
    this.tfEditable = tfEditable;
    this.tfPacsBits = tfPacsBits;
    this.tfFormat = tfFormat;
    this.tfNameConcat = tfNameConcat;
    this.tagTypes = tagTypes;
    this.translations = translations;
  }

  @Id

  @Column(name = "TRUNCATION_FORMAT_ID", unique = true, nullable = false)
  public int getTruncationFormatId() {
    return this.truncationFormatId;
  }

  public void setTruncationFormatId(int truncationFormatId) {
    this.truncationFormatId = truncationFormatId;
  }

  @Column(name = "TF_NAME_ORIGINAL", columnDefinition = "NVARCHAR", nullable = false, length = 64)
  public String getTfNameOriginal() {
    return this.tfNameOriginal;
  }

  public void setTfNameOriginal(String tfNameOriginal) {
    this.tfNameOriginal = tfNameOriginal;
  }

  @Column(name = "TF_TAG_MASK", nullable = false, length = 64)
  public String getTfTagMask() {
    return this.tfTagMask;
  }

  public void setTfTagMask(String tfTagMask) {
    this.tfTagMask = tfTagMask;
  }

  @Column(name = "TF_SITE_MASK", nullable = false, length = 64)
  public String getTfSiteMask() {
    return this.tfSiteMask;
  }

  public void setTfSiteMask(String tfSiteMask) {
    this.tfSiteMask = tfSiteMask;
  }

  @Column(name = "TF_DROP_ZERO", nullable = false)
  public int getTfDropZero() {
    return this.tfDropZero;
  }

  public void setTfDropZero(int tfDropZero) {
    this.tfDropZero = tfDropZero;
  }

  @Column(name = "TF_EDITABLE", nullable = false)
  public int getTfEditable() {
    return this.tfEditable;
  }

  public void setTfEditable(int tfEditable) {
    this.tfEditable = tfEditable;
  }

  @Column(name = "TF_PACS_BITS", nullable = false)
  public int getTfPacsBits() {
    return this.tfPacsBits;
  }

  public void setTfPacsBits(int tfPacsBits) {
    this.tfPacsBits = tfPacsBits;
  }

  @Column(name = "TF_FORMAT", length = 2048)
  public String getTfFormat() {
    return this.tfFormat;
  }

  public void setTfFormat(String tfFormat) {
    this.tfFormat = tfFormat;
  }

  @Column(name = "TF_NAME_CONCAT", columnDefinition = "NVARCHAR", length = 1024)
  public String getTfNameConcat() {
    return this.tfNameConcat;
  }

  public void setTfNameConcat(String tfNameConcat) {
    this.tfNameConcat = tfNameConcat;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "truncationFormat")
  public Set<TagType> getTagTypes() {
    return this.tagTypes;
  }

  public void setTagTypes(Set<TagType> tagTypes) {
    this.tagTypes = tagTypes;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "truncationFormat")
  public Set<Translation> getTranslations() {
    return this.translations;
  }

  public void setTranslations(Set<Translation> translations) {
    this.translations = translations;
  }

}
