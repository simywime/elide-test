package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.yahoo.elide.annotation.Exclude;
import com.yahoo.elide.annotation.Include;

/**
 * MenuItem generated by hbm2java
 */
@Entity
@Table(name = "MENU_ITEM")
@Include(rootLevel = true, type="menuItem")
public class MenuItem implements java.io.Serializable {

  private int menuItemId;
  private RegisteredView registeredView;
  private SettingType settingType;
  private String menuItemTitle;
  private String menuItemDescription;
  private Set<SettingData> settingDatas = new HashSet<SettingData>(0);
  private Set<MenuGroupItem> menuGroupItems = new HashSet<MenuGroupItem>(0);

  public MenuItem() {}

  public MenuItem(int menuItemId, RegisteredView registeredView, SettingType settingType, String menuItemTitle, String menuItemDescription) {
    this.menuItemId = menuItemId;
    this.registeredView = registeredView;
    this.settingType = settingType;
    this.menuItemTitle = menuItemTitle;
    this.menuItemDescription = menuItemDescription;
  }

  public MenuItem(int menuItemId, RegisteredView registeredView, SettingType settingType, String menuItemTitle, String menuItemDescription, Set<SettingData> settingDatas, Set<MenuGroupItem> menuGroupItems) {
    this.menuItemId = menuItemId;
    this.registeredView = registeredView;
    this.settingType = settingType;
    this.menuItemTitle = menuItemTitle;
    this.menuItemDescription = menuItemDescription;
    this.settingDatas = settingDatas;
    this.menuGroupItems = menuGroupItems;
  }

  @Id

  @Column(name = "MENU_ITEM_ID", unique = true, nullable = false)
  public int getMenuItemId() {
    return this.menuItemId;
  }

  public void setMenuItemId(int menuItemId) {
    this.menuItemId = menuItemId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "REGISTERED_VIEW_ID", nullable = false)
  public RegisteredView getRegisteredView() {
    return this.registeredView;
  }

  public void setRegisteredView(RegisteredView registeredView) {
    this.registeredView = registeredView;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SETTING_TYPE_ID", nullable = false)
  public SettingType getSettingType() {
    return this.settingType;
  }

  public void setSettingType(SettingType settingType) {
    this.settingType = settingType;
  }

  @Column(name = "MENU_ITEM_TITLE", nullable = false, length = 64)
  public String getMenuItemTitle() {
    return this.menuItemTitle;
  }

  public void setMenuItemTitle(String menuItemTitle) {
    this.menuItemTitle = menuItemTitle;
  }

  @Column(name = "MENU_ITEM_DESCRIPTION", nullable = false, length = 100)
  public String getMenuItemDescription() {
    return this.menuItemDescription;
  }

  public void setMenuItemDescription(String menuItemDescription) {
    this.menuItemDescription = menuItemDescription;
  }

  @Exclude
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "menuItem")
  public Set<SettingData> getSettingDatas() {
    return this.settingDatas;
  }

  public void setSettingDatas(Set<SettingData> settingDatas) {
    this.settingDatas = settingDatas;
  }

  @Exclude
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "menuItem")
  public Set<MenuGroupItem> getMenuGroupItems() {
    return this.menuGroupItems;
  }

  public void setMenuGroupItems(Set<MenuGroupItem> menuGroupItems) {
    this.menuGroupItems = menuGroupItems;
  }

}
