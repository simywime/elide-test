package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * UiType generated by hbm2java
 */
@Entity
@Table(name = "UI_TYPE")
public class UiType implements java.io.Serializable {

  private int uiTypeId;
  private String utName;
  private Set<Configuration> configurations = new HashSet<Configuration>(0);
  private Set<Element> elements = new HashSet<Element>(0);

  public UiType() {}

  public UiType(int uiTypeId, String utName) {
    this.uiTypeId = uiTypeId;
    this.utName = utName;
  }

  public UiType(int uiTypeId, String utName, Set<Configuration> configurations, Set<Element> elements) {
    this.uiTypeId = uiTypeId;
    this.utName = utName;
    this.configurations = configurations;
    this.elements = elements;
  }

  @Id

  @Column(name = "UI_TYPE_ID", unique = true, nullable = false)
  public int getUiTypeId() {
    return this.uiTypeId;
  }

  public void setUiTypeId(int uiTypeId) {
    this.uiTypeId = uiTypeId;
  }

  @Column(name = "UT_NAME", nullable = false, length = 64)
  public String getUtName() {
    return this.utName;
  }

  public void setUtName(String utName) {
    this.utName = utName;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "uiType")
  public Set<Configuration> getConfigurations() {
    return this.configurations;
  }

  public void setConfigurations(Set<Configuration> configurations) {
    this.configurations = configurations;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "uiType")
  public Set<Element> getElements() {
    return this.elements;
  }

  public void setElements(Set<Element> elements) {
    this.elements = elements;
  }

}
