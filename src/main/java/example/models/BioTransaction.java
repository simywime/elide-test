package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * BioTransaction generated by hbm2java
 */
@Entity
@Table(name = "BIO_TRANSACTION", uniqueConstraints = @UniqueConstraint(columnNames = {"BTR_DATE", "BTR_TIME", "BTR_SERIAL"}))
public class BioTransaction implements java.io.Serializable {

  private int bioTransactionId;
  private int btrDate;
  private int btrTime;
  private String btrSerial;
  private int bioTypeId;
  private int btrType;
  private String btrCol1;
  private String btrCol2;
  private String btrCol3;
  private String btrCol4;
  private String btrCol5;
  private String btrCol6;
  private String btrCol7;
  private String btrCol8;
  private String btrCol9;
  private String btrCol10;

  public BioTransaction() {}

  public BioTransaction(int bioTransactionId, int btrDate, int btrTime, String btrSerial, int bioTypeId, int btrType) {
    this.bioTransactionId = bioTransactionId;
    this.btrDate = btrDate;
    this.btrTime = btrTime;
    this.btrSerial = btrSerial;
    this.bioTypeId = bioTypeId;
    this.btrType = btrType;
  }

  public BioTransaction(int bioTransactionId, int btrDate, int btrTime, String btrSerial, int bioTypeId, int btrType, String btrCol1, String btrCol2, String btrCol3, String btrCol4, String btrCol5, String btrCol6, String btrCol7, String btrCol8, String btrCol9, String btrCol10) {
    this.bioTransactionId = bioTransactionId;
    this.btrDate = btrDate;
    this.btrTime = btrTime;
    this.btrSerial = btrSerial;
    this.bioTypeId = bioTypeId;
    this.btrType = btrType;
    this.btrCol1 = btrCol1;
    this.btrCol2 = btrCol2;
    this.btrCol3 = btrCol3;
    this.btrCol4 = btrCol4;
    this.btrCol5 = btrCol5;
    this.btrCol6 = btrCol6;
    this.btrCol7 = btrCol7;
    this.btrCol8 = btrCol8;
    this.btrCol9 = btrCol9;
    this.btrCol10 = btrCol10;
  }

  @Id

  @Column(name = "BIO_TRANSACTION_ID", unique = true, nullable = false)
  public int getBioTransactionId() {
    return this.bioTransactionId;
  }

  public void setBioTransactionId(int bioTransactionId) {
    this.bioTransactionId = bioTransactionId;
  }

  @Column(name = "BTR_DATE", nullable = false)
  public int getBtrDate() {
    return this.btrDate;
  }

  public void setBtrDate(int btrDate) {
    this.btrDate = btrDate;
  }

  @Column(name = "BTR_TIME", nullable = false)
  public int getBtrTime() {
    return this.btrTime;
  }

  public void setBtrTime(int btrTime) {
    this.btrTime = btrTime;
  }

  @Column(name = "BTR_SERIAL", nullable = false, length = 64)
  public String getBtrSerial() {
    return this.btrSerial;
  }

  public void setBtrSerial(String btrSerial) {
    this.btrSerial = btrSerial;
  }

  @Column(name = "BIO_TYPE_ID", nullable = false)
  public int getBioTypeId() {
    return this.bioTypeId;
  }

  public void setBioTypeId(int bioTypeId) {
    this.bioTypeId = bioTypeId;
  }

  @Column(name = "BTR_TYPE", nullable = false)
  public int getBtrType() {
    return this.btrType;
  }

  public void setBtrType(int btrType) {
    this.btrType = btrType;
  }

  @Column(name = "BTR_COL1", length = 64)
  public String getBtrCol1() {
    return this.btrCol1;
  }

  public void setBtrCol1(String btrCol1) {
    this.btrCol1 = btrCol1;
  }

  @Column(name = "BTR_COL2", length = 64)
  public String getBtrCol2() {
    return this.btrCol2;
  }

  public void setBtrCol2(String btrCol2) {
    this.btrCol2 = btrCol2;
  }

  @Column(name = "BTR_COL3", length = 64)
  public String getBtrCol3() {
    return this.btrCol3;
  }

  public void setBtrCol3(String btrCol3) {
    this.btrCol3 = btrCol3;
  }

  @Column(name = "BTR_COL4", length = 64)
  public String getBtrCol4() {
    return this.btrCol4;
  }

  public void setBtrCol4(String btrCol4) {
    this.btrCol4 = btrCol4;
  }

  @Column(name = "BTR_COL5", length = 64)
  public String getBtrCol5() {
    return this.btrCol5;
  }

  public void setBtrCol5(String btrCol5) {
    this.btrCol5 = btrCol5;
  }

  @Column(name = "BTR_COL6", length = 64)
  public String getBtrCol6() {
    return this.btrCol6;
  }

  public void setBtrCol6(String btrCol6) {
    this.btrCol6 = btrCol6;
  }

  @Column(name = "BTR_COL7", length = 64)
  public String getBtrCol7() {
    return this.btrCol7;
  }

  public void setBtrCol7(String btrCol7) {
    this.btrCol7 = btrCol7;
  }

  @Column(name = "BTR_COL8", length = 64)
  public String getBtrCol8() {
    return this.btrCol8;
  }

  public void setBtrCol8(String btrCol8) {
    this.btrCol8 = btrCol8;
  }

  @Column(name = "BTR_COL9", length = 64)
  public String getBtrCol9() {
    return this.btrCol9;
  }

  public void setBtrCol9(String btrCol9) {
    this.btrCol9 = btrCol9;
  }

  @Column(name = "BTR_COL10", length = 64)
  public String getBtrCol10() {
    return this.btrCol10;
  }

  public void setBtrCol10(String btrCol10) {
    this.btrCol10 = btrCol10;
  }

}
