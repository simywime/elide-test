package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * BioTemplatePk generated by hbm2java
 */
@Entity
@Table(name = "BIO_TEMPLATE_PK", uniqueConstraints = @UniqueConstraint(columnNames = "BTP_NAME"))
public class BioTemplatePk implements java.io.Serializable {

  private int bioTemplatePkId;
  private String btpName;
  private String btpPublicKey;
  private String btpKeyReference;
  private Set<BioTemplateSk> bioTemplateSks = new HashSet<BioTemplateSk>(0);

  public BioTemplatePk() {}

  public BioTemplatePk(int bioTemplatePkId, String btpName, String btpPublicKey) {
    this.bioTemplatePkId = bioTemplatePkId;
    this.btpName = btpName;
    this.btpPublicKey = btpPublicKey;
  }

  public BioTemplatePk(int bioTemplatePkId, String btpName, String btpPublicKey, String btpKeyReference, Set<BioTemplateSk> bioTemplateSks) {
    this.bioTemplatePkId = bioTemplatePkId;
    this.btpName = btpName;
    this.btpPublicKey = btpPublicKey;
    this.btpKeyReference = btpKeyReference;
    this.bioTemplateSks = bioTemplateSks;
  }

  @Id

  @Column(name = "BIO_TEMPLATE_PK_ID", unique = true, nullable = false)
  public int getBioTemplatePkId() {
    return this.bioTemplatePkId;
  }

  public void setBioTemplatePkId(int bioTemplatePkId) {
    this.bioTemplatePkId = bioTemplatePkId;
  }

  @Column(name = "BTP_NAME", unique = true, nullable = false, length = 64)
  public String getBtpName() {
    return this.btpName;
  }

  public void setBtpName(String btpName) {
    this.btpName = btpName;
  }

  @Column(name = "BTP_PUBLIC_KEY", nullable = false, length = 1024)
  public String getBtpPublicKey() {
    return this.btpPublicKey;
  }

  public void setBtpPublicKey(String btpPublicKey) {
    this.btpPublicKey = btpPublicKey;
  }

  @Column(name = "BTP_KEY_REFERENCE", length = 256)
  public String getBtpKeyReference() {
    return this.btpKeyReference;
  }

  public void setBtpKeyReference(String btpKeyReference) {
    this.btpKeyReference = btpKeyReference;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "bioTemplatePk")
  public Set<BioTemplateSk> getBioTemplateSks() {
    return this.bioTemplateSks;
  }

  public void setBioTemplateSks(Set<BioTemplateSk> bioTemplateSks) {
    this.bioTemplateSks = bioTemplateSks;
  }

}
