package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * DevAction generated by hbm2java
 */
@Entity
@Table(name = "DEV_ACTION", uniqueConstraints = @UniqueConstraint(columnNames = {"DEVICE_ACTION_ID", "SETTING_ID"}))
public class DevAction implements java.io.Serializable {

  private int devActionId;
  private DeviceAction deviceAction;
  private DeviceImprox deviceImprox;
  private Setting setting;
  private int devActionNo;
  private Set<AccessGroupAction> accessGroupActions = new HashSet<AccessGroupAction>(0);
  private Set<TimeTriggeredAction> timeTriggeredActions = new HashSet<TimeTriggeredAction>(0);
  private Set<DevEventAction> devEventActions = new HashSet<DevEventAction>(0);

  public DevAction() {}

  public DevAction(int devActionId, DeviceAction deviceAction, DeviceImprox deviceImprox, Setting setting, int devActionNo) {
    this.devActionId = devActionId;
    this.deviceAction = deviceAction;
    this.deviceImprox = deviceImprox;
    this.setting = setting;
    this.devActionNo = devActionNo;
  }

  public DevAction(int devActionId, DeviceAction deviceAction, DeviceImprox deviceImprox, Setting setting, int devActionNo, Set<AccessGroupAction> accessGroupActions, Set<TimeTriggeredAction> timeTriggeredActions, Set<DevEventAction> devEventActions) {
    this.devActionId = devActionId;
    this.deviceAction = deviceAction;
    this.deviceImprox = deviceImprox;
    this.setting = setting;
    this.devActionNo = devActionNo;
    this.accessGroupActions = accessGroupActions;
    this.timeTriggeredActions = timeTriggeredActions;
    this.devEventActions = devEventActions;
  }

  @Id

  @Column(name = "DEV_ACTION_ID", unique = true, nullable = false)
  public int getDevActionId() {
    return this.devActionId;
  }

  public void setDevActionId(int devActionId) {
    this.devActionId = devActionId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "DEVICE_ACTION_ID", nullable = false)
  public DeviceAction getDeviceAction() {
    return this.deviceAction;
  }

  public void setDeviceAction(DeviceAction deviceAction) {
    this.deviceAction = deviceAction;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "DEVICE_IMPROX_ID", nullable = false)
  public DeviceImprox getDeviceImprox() {
    return this.deviceImprox;
  }

  public void setDeviceImprox(DeviceImprox deviceImprox) {
    this.deviceImprox = deviceImprox;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SETTING_ID", nullable = false)
  public Setting getSetting() {
    return this.setting;
  }

  public void setSetting(Setting setting) {
    this.setting = setting;
  }

  @Column(name = "DEV_ACTION_NO", nullable = false)
  public int getDevActionNo() {
    return this.devActionNo;
  }

  public void setDevActionNo(int devActionNo) {
    this.devActionNo = devActionNo;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "devAction")
  public Set<AccessGroupAction> getAccessGroupActions() {
    return this.accessGroupActions;
  }

  public void setAccessGroupActions(Set<AccessGroupAction> accessGroupActions) {
    this.accessGroupActions = accessGroupActions;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "devAction")
  public Set<TimeTriggeredAction> getTimeTriggeredActions() {
    return this.timeTriggeredActions;
  }

  public void setTimeTriggeredActions(Set<TimeTriggeredAction> timeTriggeredActions) {
    this.timeTriggeredActions = timeTriggeredActions;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "devAction")
  public Set<DevEventAction> getDevEventActions() {
    return this.devEventActions;
  }

  public void setDevEventActions(Set<DevEventAction> devEventActions) {
    this.devEventActions = devEventActions;
  }

}
