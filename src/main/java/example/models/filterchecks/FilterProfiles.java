package example.models.filterchecks;

import java.util.*;

import com.yahoo.elide.annotation.SecurityCheck;
import com.yahoo.elide.core.Path;
import com.yahoo.elide.core.filter.FilterPredicate;
import com.yahoo.elide.core.filter.Operator;
import com.yahoo.elide.core.filter.expression.*;
import com.yahoo.elide.core.filter.expression.AndFilterExpression;
import com.yahoo.elide.security.FilterExpressionCheck;
import com.yahoo.elide.security.RequestScope;

import example.models.Profile;

/**
 * This filter is applied to the Profile domain using @ReadPermission.
 * 
 * <p>
 * It filters any request to read profiles by the following expressions:
 * <ol>1. PSysname of either: DEFAULT or USER_DEFINED</ol>
 * <ol>2. PHidden of 0</ol>
 * </p>
 */
@SecurityCheck(FilterProfiles.CHECK_SYSNAME)
public class FilterProfiles extends FilterExpressionCheck<Profile> {
  public static final String CHECK_SYSNAME = "Profile READ filters";

  /* (non-Javadoc)
   * @see com.yahoo.elide.security.FilterExpressionCheck#getFilterExpression(java.lang.Class, com.yahoo.elide.security.RequestScope)
   */
  @Override
  public FilterExpression getFilterExpression(Class<?> entityClass, RequestScope requestScope) {
    FilterExpression filter = null;
    
    // Filter: PHidden of 0 only. I.e. exclude hidden profiles from results.
    Path.PathElement pathElePHidden = new Path.PathElement(Profile.class, Integer.class, "profileHidden");
    Path pathPHidden = new Path(Arrays.asList(pathElePHidden));
    FilterExpression filterByPHidden = new FilterPredicate(pathPHidden, Operator.IN, Collections.singletonList(0));
    
    // AND the filters
    filter = new AndFilterExpression(filterByPHidden, getPSysnameFilter());
    
    return filter;
  }
  
  /**
   * PSysname filters to limit results to one of: DEFAULT, USER_DEFINED.
   * @return
   */
  private FilterExpression getPSysnameFilter() {
    FilterExpression filter = null;
    
    // Filter: DEFAULT PSysname.
    Path.PathElement pathElePSysnameDef = new Path.PathElement(Profile.class, String.class, "profileSysname");
    Path pathPSysnameDef = new Path(Arrays.asList(pathElePSysnameDef));
    FilterExpression filterBySysnameDef = new FilterPredicate(pathPSysnameDef, Operator.IN, Collections.singletonList("DEFAULT"));
    
    // Filter: USER_DEFINED PSysname.
    Path.PathElement pathElePSysnameUserDef = new Path.PathElement(Profile.class, String.class, "profileSysname");
    Path pathPSysnameUserDef = new Path(Arrays.asList(pathElePSysnameUserDef));
    FilterExpression filterBySysnameUserDef = new FilterPredicate(pathPSysnameUserDef, Operator.IN, Collections.singletonList("USER_DEFINED"));
    
    // OR the filters
    filter = new OrFilterExpression(filterBySysnameDef, filterBySysnameUserDef);
    
    return filter;
  }
  
}
