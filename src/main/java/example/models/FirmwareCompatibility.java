package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * FirmwareCompatibility generated by hbm2java
 */
@Entity
@Table(name = "FIRMWARE_COMPATIBILITY", uniqueConstraints = @UniqueConstraint(columnNames = {"UNIT_TYPE_ID", "FW_BOOT_VERSION"}))
public class FirmwareCompatibility implements java.io.Serializable {

  private int firmwareCompatibilityId;
  private FirmwareFiles firmwareFilesByFwFilesRemoteId;
  private FirmwareFiles firmwareFilesByFwFilesDeviceImproxId;
  private UnitType unitType;
  private int fwBootVersion;

  public FirmwareCompatibility() {}

  public FirmwareCompatibility(int firmwareCompatibilityId, UnitType unitType, int fwBootVersion) {
    this.firmwareCompatibilityId = firmwareCompatibilityId;
    this.unitType = unitType;
    this.fwBootVersion = fwBootVersion;
  }

  public FirmwareCompatibility(int firmwareCompatibilityId, FirmwareFiles firmwareFilesByFwFilesRemoteId, FirmwareFiles firmwareFilesByFwFilesDeviceImproxId, UnitType unitType, int fwBootVersion) {
    this.firmwareCompatibilityId = firmwareCompatibilityId;
    this.firmwareFilesByFwFilesRemoteId = firmwareFilesByFwFilesRemoteId;
    this.firmwareFilesByFwFilesDeviceImproxId = firmwareFilesByFwFilesDeviceImproxId;
    this.unitType = unitType;
    this.fwBootVersion = fwBootVersion;
  }

  @Id

  @Column(name = "FIRMWARE_COMPATIBILITY_ID", unique = true, nullable = false)
  public int getFirmwareCompatibilityId() {
    return this.firmwareCompatibilityId;
  }

  public void setFirmwareCompatibilityId(int firmwareCompatibilityId) {
    this.firmwareCompatibilityId = firmwareCompatibilityId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "FW_FILES_REMOTE_ID")
  public FirmwareFiles getFirmwareFilesByFwFilesRemoteId() {
    return this.firmwareFilesByFwFilesRemoteId;
  }

  public void setFirmwareFilesByFwFilesRemoteId(FirmwareFiles firmwareFilesByFwFilesRemoteId) {
    this.firmwareFilesByFwFilesRemoteId = firmwareFilesByFwFilesRemoteId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "FW_FILES_DEVICE_IMPROX_ID")
  public FirmwareFiles getFirmwareFilesByFwFilesDeviceImproxId() {
    return this.firmwareFilesByFwFilesDeviceImproxId;
  }

  public void setFirmwareFilesByFwFilesDeviceImproxId(FirmwareFiles firmwareFilesByFwFilesDeviceImproxId) {
    this.firmwareFilesByFwFilesDeviceImproxId = firmwareFilesByFwFilesDeviceImproxId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "UNIT_TYPE_ID", nullable = false)
  public UnitType getUnitType() {
    return this.unitType;
  }

  public void setUnitType(UnitType unitType) {
    this.unitType = unitType;
  }

  @Column(name = "FW_BOOT_VERSION", nullable = false)
  public int getFwBootVersion() {
    return this.fwBootVersion;
  }

  public void setFwBootVersion(int fwBootVersion) {
    this.fwBootVersion = fwBootVersion;
  }

}
