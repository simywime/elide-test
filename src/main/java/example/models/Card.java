package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.yahoo.elide.annotation.Exclude;
import com.yahoo.elide.annotation.Include;

/**
 * Card generated by hbm2java
 */
@Entity
@Table(name = "CARD")
@Include(type="card")
public class Card implements java.io.Serializable {

  private int cardId;
  private Site site;
  private String cardName;
  private int cardOrientation;
  private int cardPrintType;
  private String cardPath;
  private int cardUnits;
  private String cardLabelTemplate;
  private String cardQuery;
  private Set<CardFace> cardFaces = new HashSet<CardFace>(0);
  private Set<Master> masters = new HashSet<Master>(0);
  private Set<CardEncoding> cardEncodings = new HashSet<CardEncoding>(0);
  private Set<Profile> profiles = new HashSet<Profile>(0);
  private Set<Tag> tags = new HashSet<Tag>(0);

  public Card() {}

  public Card(int cardId, int cardOrientation, int cardPrintType, int cardUnits, String cardQuery) {
    this.cardId = cardId;
    this.cardOrientation = cardOrientation;
    this.cardPrintType = cardPrintType;
    this.cardUnits = cardUnits;
    this.cardQuery = cardQuery;
  }

  public Card(int cardId, Site site, String cardName, int cardOrientation, int cardPrintType, String cardPath, int cardUnits, String cardLabelTemplate, String cardQuery, Set<CardFace> cardFaces, Set<Master> masters, Set<CardEncoding> cardEncodings, Set<Profile> profiles, Set<Tag> tags) {
    this.cardId = cardId;
    this.site = site;
    this.cardName = cardName;
    this.cardOrientation = cardOrientation;
    this.cardPrintType = cardPrintType;
    this.cardPath = cardPath;
    this.cardUnits = cardUnits;
    this.cardLabelTemplate = cardLabelTemplate;
    this.cardQuery = cardQuery;
    this.cardFaces = cardFaces;
    this.masters = masters;
    this.cardEncodings = cardEncodings;
    this.profiles = profiles;
    this.tags = tags;
  }

  @Id

  @Column(name = "CARD_ID", unique = true, nullable = false)
  public int getCardId() {
    return this.cardId;
  }

  public void setCardId(int cardId) {
    this.cardId = cardId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SITE_ID")
  public Site getSite() {
    return this.site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  @Column(name = "CARD_NAME", length = 64)
  public String getCardName() {
    return this.cardName;
  }

  public void setCardName(String cardName) {
    this.cardName = cardName;
  }

  @Column(name = "CARD_ORIENTATION", nullable = false)
  public int getCardOrientation() {
    return this.cardOrientation;
  }

  public void setCardOrientation(int cardOrientation) {
    this.cardOrientation = cardOrientation;
  }

  @Column(name = "CARD_PRINT_TYPE", nullable = false)
  public int getCardPrintType() {
    return this.cardPrintType;
  }

  public void setCardPrintType(int cardPrintType) {
    this.cardPrintType = cardPrintType;
  }

  @Column(name = "CARD_PATH", length = 80)
  public String getCardPath() {
    return this.cardPath;
  }

  public void setCardPath(String cardPath) {
    this.cardPath = cardPath;
  }

  @Column(name = "CARD_UNITS", nullable = false)
  public int getCardUnits() {
    return this.cardUnits;
  }

  public void setCardUnits(int cardUnits) {
    this.cardUnits = cardUnits;
  }

  @Column(name = "CARD_LABEL_TEMPLATE", length = 64)
  public String getCardLabelTemplate() {
    return this.cardLabelTemplate;
  }

  public void setCardLabelTemplate(String cardLabelTemplate) {
    this.cardLabelTemplate = cardLabelTemplate;
  }

  @Column(name = "CARD_QUERY", nullable = false, length = 2048)
  public String getCardQuery() {
    return this.cardQuery;
  }

  public void setCardQuery(String cardQuery) {
    this.cardQuery = cardQuery;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "card")
  @Exclude
  public Set<CardFace> getCardFaces() {
    return this.cardFaces;
  }

  public void setCardFaces(Set<CardFace> cardFaces) {
    this.cardFaces = cardFaces;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "card")
  @Exclude
  public Set<Master> getMasters() {
    return this.masters;
  }

  public void setMasters(Set<Master> masters) {
    this.masters = masters;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "card")
  @Exclude
  public Set<CardEncoding> getCardEncodings() {
    return this.cardEncodings;
  }

  public void setCardEncodings(Set<CardEncoding> cardEncodings) {
    this.cardEncodings = cardEncodings;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "card")
  @Exclude
  public Set<Profile> getProfiles() {
    return this.profiles;
  }

  public void setProfiles(Set<Profile> profiles) {
    this.profiles = profiles;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "card")
  @Exclude
  public Set<Tag> getTags() {
    return this.tags;
  }

  public void setTags(Set<Tag> tags) {
    this.tags = tags;
  }

}
