package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * EventType generated by hbm2java
 */
@Entity
@Table(name = "EVENT_TYPE")
public class EventType implements java.io.Serializable {

  private int eventTypeId;
  private CapabilityRequirements capabilityRequirements;
  private int etAlarm;
  private String etName;
  private int etDirection;
  private String etDescOriginal;
  private String etSysname;
  private String etSysnameOut;
  private String etDescConcat;
  private Set<DeviceEvent> deviceEvents = new HashSet<DeviceEvent>(0);
  private Set<SettingData> settingDatas = new HashSet<SettingData>(0);
  private Set<Transack> transacks = new HashSet<Transack>(0);
  private Set<Translation> translations = new HashSet<Translation>(0);

  public EventType() {}

  public EventType(int eventTypeId, int etAlarm, String etName, int etDirection, String etDescOriginal, String etSysname, String etSysnameOut, String etDescConcat) {
    this.eventTypeId = eventTypeId;
    this.etAlarm = etAlarm;
    this.etName = etName;
    this.etDirection = etDirection;
    this.etDescOriginal = etDescOriginal;
    this.etSysname = etSysname;
    this.etSysnameOut = etSysnameOut;
    this.etDescConcat = etDescConcat;
  }

  public EventType(int eventTypeId, CapabilityRequirements capabilityRequirements, int etAlarm, String etName, int etDirection, String etDescOriginal, String etSysname, String etSysnameOut, String etDescConcat, Set<DeviceEvent> deviceEvents, Set<SettingData> settingDatas, Set<Transack> transacks, Set<Translation> translations) {
    this.eventTypeId = eventTypeId;
    this.capabilityRequirements = capabilityRequirements;
    this.etAlarm = etAlarm;
    this.etName = etName;
    this.etDirection = etDirection;
    this.etDescOriginal = etDescOriginal;
    this.etSysname = etSysname;
    this.etSysnameOut = etSysnameOut;
    this.etDescConcat = etDescConcat;
    this.deviceEvents = deviceEvents;
    this.settingDatas = settingDatas;
    this.transacks = transacks;
    this.translations = translations;
  }

  @Id

  @Column(name = "EVENT_TYPE_ID", unique = true, nullable = false)
  public int getEventTypeId() {
    return this.eventTypeId;
  }

  public void setEventTypeId(int eventTypeId) {
    this.eventTypeId = eventTypeId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CAPABILITY_REQUIREMENTS_ID")
  public CapabilityRequirements getCapabilityRequirements() {
    return this.capabilityRequirements;
  }

  public void setCapabilityRequirements(CapabilityRequirements capabilityRequirements) {
    this.capabilityRequirements = capabilityRequirements;
  }

  @Column(name = "ET_ALARM", nullable = false)
  public int getEtAlarm() {
    return this.etAlarm;
  }

  public void setEtAlarm(int etAlarm) {
    this.etAlarm = etAlarm;
  }

  @Column(name = "ET_NAME", nullable = false, length = 64)
  public String getEtName() {
    return this.etName;
  }

  public void setEtName(String etName) {
    this.etName = etName;
  }

  @Column(name = "ET_DIRECTION", nullable = false)
  public int getEtDirection() {
    return this.etDirection;
  }

  public void setEtDirection(int etDirection) {
    this.etDirection = etDirection;
  }

  @Column(name = "ET_DESC_ORIGINAL", columnDefinition = "NVARCHAR", nullable = false, length = 64)
  public String getEtDescOriginal() {
    return this.etDescOriginal;
  }

  public void setEtDescOriginal(String etDescOriginal) {
    this.etDescOriginal = etDescOriginal;
  }

  @Column(name = "ET_SYSNAME", nullable = false, length = 64)
  public String getEtSysname() {
    return this.etSysname;
  }

  public void setEtSysname(String etSysname) {
    this.etSysname = etSysname;
  }

  @Column(name = "ET_SYSNAME_OUT", nullable = false, length = 64)
  public String getEtSysnameOut() {
    return this.etSysnameOut;
  }

  public void setEtSysnameOut(String etSysnameOut) {
    this.etSysnameOut = etSysnameOut;
  }

  @Column(name = "ET_DESC_CONCAT", columnDefinition = "NVARCHAR", nullable = false, length = 256)
  public String getEtDescConcat() {
    return this.etDescConcat;
  }

  public void setEtDescConcat(String etDescConcat) {
    this.etDescConcat = etDescConcat;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventType")
  public Set<DeviceEvent> getDeviceEvents() {
    return this.deviceEvents;
  }

  public void setDeviceEvents(Set<DeviceEvent> deviceEvents) {
    this.deviceEvents = deviceEvents;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventType")
  public Set<SettingData> getSettingDatas() {
    return this.settingDatas;
  }

  public void setSettingDatas(Set<SettingData> settingDatas) {
    this.settingDatas = settingDatas;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventType")
  public Set<Transack> getTransacks() {
    return this.transacks;
  }

  public void setTransacks(Set<Transack> transacks) {
    this.transacks = transacks;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventType")
  public Set<Translation> getTranslations() {
    return this.translations;
  }

  public void setTranslations(Set<Translation> translations) {
    this.translations = translations;
  }

}
