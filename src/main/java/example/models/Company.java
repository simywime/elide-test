package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.yahoo.elide.annotation.Include;

/**
 * Company generated by hbm2java
 */
@Entity
@Table(name = "COMPANY")
@Include(rootLevel = false, type="company")
public class Company implements java.io.Serializable {

  private int companyId;
  private Site site;
  private String compName;
  private int compDefault;
  private Set<SettingData> settingDatas = new HashSet<SettingData>(0);
  private Set<Transack> transacks = new HashSet<Transack>(0);
  private Set<Master> masters = new HashSet<Master>(0);

  public Company() {}

  public Company(int companyId, Site site, String compName, int compDefault) {
    this.companyId = companyId;
    this.site = site;
    this.compName = compName;
    this.compDefault = compDefault;
  }

  public Company(int companyId, Site site, String compName, int compDefault, Set<SettingData> settingDatas, Set<Transack> transacks, Set<Master> masters) {
    this.companyId = companyId;
    this.site = site;
    this.compName = compName;
    this.compDefault = compDefault;
    this.settingDatas = settingDatas;
    this.transacks = transacks;
    this.masters = masters;
  }

  @Id

  @Column(name = "COMPANY_ID", unique = true, nullable = false)
  public int getCompanyId() {
    return this.companyId;
  }

  public void setCompanyId(int companyId) {
    this.companyId = companyId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SITE_ID", nullable = false)
  public Site getSite() {
    return this.site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  @Column(name = "COMP_NAME", nullable = false, length = 64)
  public String getCompName() {
    return this.compName;
  }

  public void setCompName(String compName) {
    this.compName = compName;
  }

  @Column(name = "COMP_DEFAULT", nullable = false)
  public int getCompDefault() {
    return this.compDefault;
  }

  public void setCompDefault(int compDefault) {
    this.compDefault = compDefault;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
  public Set<SettingData> getSettingDatas() {
    return this.settingDatas;
  }

  public void setSettingDatas(Set<SettingData> settingDatas) {
    this.settingDatas = settingDatas;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
  public Set<Transack> getTransacks() {
    return this.transacks;
  }

  public void setTransacks(Set<Transack> transacks) {
    this.transacks = transacks;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
  public Set<Master> getMasters() {
    return this.masters;
  }

  public void setMasters(Set<Master> masters) {
    this.masters = masters;
  }

}
