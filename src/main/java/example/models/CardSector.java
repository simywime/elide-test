package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * CardSector generated by hbm2java
 */
@Entity
@Table(name = "CARD_SECTOR")
public class CardSector implements java.io.Serializable {

  private int cardSectorId;
  private CardBlock cardBlock;
  private int csOrdinal;
  private Set<CardEncodingData> cardEncodingDatas = new HashSet<CardEncodingData>(0);

  public CardSector() {}

  public CardSector(int cardSectorId, CardBlock cardBlock, int csOrdinal) {
    this.cardSectorId = cardSectorId;
    this.cardBlock = cardBlock;
    this.csOrdinal = csOrdinal;
  }

  public CardSector(int cardSectorId, CardBlock cardBlock, int csOrdinal, Set<CardEncodingData> cardEncodingDatas) {
    this.cardSectorId = cardSectorId;
    this.cardBlock = cardBlock;
    this.csOrdinal = csOrdinal;
    this.cardEncodingDatas = cardEncodingDatas;
  }

  @Id

  @Column(name = "CARD_SECTOR_ID", unique = true, nullable = false)
  public int getCardSectorId() {
    return this.cardSectorId;
  }

  public void setCardSectorId(int cardSectorId) {
    this.cardSectorId = cardSectorId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CARD_BLOCK_ID", nullable = false)
  public CardBlock getCardBlock() {
    return this.cardBlock;
  }

  public void setCardBlock(CardBlock cardBlock) {
    this.cardBlock = cardBlock;
  }

  @Column(name = "CS_ORDINAL", nullable = false)
  public int getCsOrdinal() {
    return this.csOrdinal;
  }

  public void setCsOrdinal(int csOrdinal) {
    this.csOrdinal = csOrdinal;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "cardSector")
  public Set<CardEncodingData> getCardEncodingDatas() {
    return this.cardEncodingDatas;
  }

  public void setCardEncodingDatas(Set<CardEncodingData> cardEncodingDatas) {
    this.cardEncodingDatas = cardEncodingDatas;
  }

}
