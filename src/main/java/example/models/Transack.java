package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Transack generated by hbm2java
 */
@Entity
@Table(name = "TRANSACK", uniqueConstraints = @UniqueConstraint(columnNames = {"TR_SQ", "TR_DATETIMEUTC", "TR_SLA"}))
public class Transack implements java.io.Serializable {

  private int transackId;
  private Building building;
  private CommonZone commonZone;
  private Company company;
  private Controller controller;
  private Department department;
  private EventType eventType;
  private Floor floor;
  private Location location;
  private Master masterByTrMasterId;
  private Master masterByTrHostid;
  private Master masterByAckMasterId;
  private Site site;
  private Terminal terminal;
  private TransackData transackData;
  private TransackType transackType;
  private UnitType unitType;
  private Zone zone;
  private int trSq;
  private String trDatetimeutc;
  private String trDatetimelocal;
  private Integer trDirection;
  private String trData;
  private String trCtrlName;
  private String trSla;
  private String trDevName;
  private String trDevFixedaddr;
  private String trDevSerial;
  private String trBuildingName;
  private String trFloorName;
  private String trLocName;
  private String trZoneName;
  private String trCzName;
  private String trDeptName;
  private String trCompName;
  private String trTagcode;
  private Integer trTagTypeId;
  private Integer trReasoncode;
  private String trProcessed;
  private String ackDatetime;
  private String ackRepsonse;
  private String trSiteName;
  private int trInjected;
  private Integer trNestedZone;
  private String trReasonCodeName;
  private int trTagcodeNumBits;
  private Integer trExpiryReasonId;
  private String trExpiryReasonName;
  private Set<MasterAsset> masterAssetsForMaFirstAllowedTrnId = new HashSet<MasterAsset>(0);
  private Set<MasterAsset> masterAssetsForMaLastAllowedTrnId = new HashSet<MasterAsset>(0);

  public Transack() {}

  public Transack(int transackId, EventType eventType, TransackType transackType, UnitType unitType, int trSq, String trDatetimeutc, String trDatetimelocal, String trCtrlName, String trSla, String trDevFixedaddr, String trDevSerial, int trInjected, int trTagcodeNumBits) {
    this.transackId = transackId;
    this.eventType = eventType;
    this.transackType = transackType;
    this.unitType = unitType;
    this.trSq = trSq;
    this.trDatetimeutc = trDatetimeutc;
    this.trDatetimelocal = trDatetimelocal;
    this.trCtrlName = trCtrlName;
    this.trSla = trSla;
    this.trDevFixedaddr = trDevFixedaddr;
    this.trDevSerial = trDevSerial;
    this.trInjected = trInjected;
    this.trTagcodeNumBits = trTagcodeNumBits;
  }

  public Transack(int transackId, Building building, CommonZone commonZone, Company company, Controller controller, Department department, EventType eventType, Floor floor, Location location, Master masterByTrMasterId, Master masterByTrHostid, Master masterByAckMasterId, Site site, Terminal terminal, TransackData transackData, TransackType transackType, UnitType unitType, Zone zone, int trSq, String trDatetimeutc, String trDatetimelocal, Integer trDirection, String trData, String trCtrlName, String trSla, String trDevName, String trDevFixedaddr, String trDevSerial, String trBuildingName, String trFloorName, String trLocName, String trZoneName, String trCzName, String trDeptName, String trCompName, String trTagcode, Integer trTagTypeId, Integer trReasoncode, String trProcessed, String ackDatetime, String ackRepsonse, String trSiteName, int trInjected, Integer trNestedZone, String trReasonCodeName, int trTagcodeNumBits, Integer trExpiryReasonId, String trExpiryReasonName, Set<MasterAsset> masterAssetsForMaFirstAllowedTrnId, Set<MasterAsset> masterAssetsForMaLastAllowedTrnId) {
    this.transackId = transackId;
    this.building = building;
    this.commonZone = commonZone;
    this.company = company;
    this.controller = controller;
    this.department = department;
    this.eventType = eventType;
    this.floor = floor;
    this.location = location;
    this.masterByTrMasterId = masterByTrMasterId;
    this.masterByTrHostid = masterByTrHostid;
    this.masterByAckMasterId = masterByAckMasterId;
    this.site = site;
    this.terminal = terminal;
    this.transackData = transackData;
    this.transackType = transackType;
    this.unitType = unitType;
    this.zone = zone;
    this.trSq = trSq;
    this.trDatetimeutc = trDatetimeutc;
    this.trDatetimelocal = trDatetimelocal;
    this.trDirection = trDirection;
    this.trData = trData;
    this.trCtrlName = trCtrlName;
    this.trSla = trSla;
    this.trDevName = trDevName;
    this.trDevFixedaddr = trDevFixedaddr;
    this.trDevSerial = trDevSerial;
    this.trBuildingName = trBuildingName;
    this.trFloorName = trFloorName;
    this.trLocName = trLocName;
    this.trZoneName = trZoneName;
    this.trCzName = trCzName;
    this.trDeptName = trDeptName;
    this.trCompName = trCompName;
    this.trTagcode = trTagcode;
    this.trTagTypeId = trTagTypeId;
    this.trReasoncode = trReasoncode;
    this.trProcessed = trProcessed;
    this.ackDatetime = ackDatetime;
    this.ackRepsonse = ackRepsonse;
    this.trSiteName = trSiteName;
    this.trInjected = trInjected;
    this.trNestedZone = trNestedZone;
    this.trReasonCodeName = trReasonCodeName;
    this.trTagcodeNumBits = trTagcodeNumBits;
    this.trExpiryReasonId = trExpiryReasonId;
    this.trExpiryReasonName = trExpiryReasonName;
    this.masterAssetsForMaFirstAllowedTrnId = masterAssetsForMaFirstAllowedTrnId;
    this.masterAssetsForMaLastAllowedTrnId = masterAssetsForMaLastAllowedTrnId;
  }

  @Id

  @Column(name = "TRANSACK_ID", unique = true, nullable = false)
  public int getTransackId() {
    return this.transackId;
  }

  public void setTransackId(int transackId) {
    this.transackId = transackId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_BUILDING_ID")
  public Building getBuilding() {
    return this.building;
  }

  public void setBuilding(Building building) {
    this.building = building;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_COMMON_ZONE_ID")
  public CommonZone getCommonZone() {
    return this.commonZone;
  }

  public void setCommonZone(CommonZone commonZone) {
    this.commonZone = commonZone;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_COMPANY_ID")
  public Company getCompany() {
    return this.company;
  }

  public void setCompany(Company company) {
    this.company = company;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_CONTROLLER_ID")
  public Controller getController() {
    return this.controller;
  }

  public void setController(Controller controller) {
    this.controller = controller;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_DEPARTMENT_ID")
  public Department getDepartment() {
    return this.department;
  }

  public void setDepartment(Department department) {
    this.department = department;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_EVENT_TYPE_ID", nullable = false)
  public EventType getEventType() {
    return this.eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_FLOOR_ID")
  public Floor getFloor() {
    return this.floor;
  }

  public void setFloor(Floor floor) {
    this.floor = floor;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_LOCATION_ID")
  public Location getLocation() {
    return this.location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_MASTER_ID")
  public Master getMasterByTrMasterId() {
    return this.masterByTrMasterId;
  }

  public void setMasterByTrMasterId(Master masterByTrMasterId) {
    this.masterByTrMasterId = masterByTrMasterId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_HOSTID")
  public Master getMasterByTrHostid() {
    return this.masterByTrHostid;
  }

  public void setMasterByTrHostid(Master masterByTrHostid) {
    this.masterByTrHostid = masterByTrHostid;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ACK_MASTER_ID")
  public Master getMasterByAckMasterId() {
    return this.masterByAckMasterId;
  }

  public void setMasterByAckMasterId(Master masterByAckMasterId) {
    this.masterByAckMasterId = masterByAckMasterId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_SITE_ID")
  public Site getSite() {
    return this.site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_TERMINAL_ID")
  public Terminal getTerminal() {
    return this.terminal;
  }

  public void setTerminal(Terminal terminal) {
    this.terminal = terminal;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_TRANSACK_DATA_ID")
  public TransackData getTransackData() {
    return this.transackData;
  }

  public void setTransackData(TransackData transackData) {
    this.transackData = transackData;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TRANSACK_TYPE_ID", nullable = false)
  public TransackType getTransackType() {
    return this.transackType;
  }

  public void setTransackType(TransackType transackType) {
    this.transackType = transackType;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_UNIT_TYPE_ID", nullable = false)
  public UnitType getUnitType() {
    return this.unitType;
  }

  public void setUnitType(UnitType unitType) {
    this.unitType = unitType;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "TR_ZONE_ID")
  public Zone getZone() {
    return this.zone;
  }

  public void setZone(Zone zone) {
    this.zone = zone;
  }

  @Column(name = "TR_SQ", nullable = false)
  public int getTrSq() {
    return this.trSq;
  }

  public void setTrSq(int trSq) {
    this.trSq = trSq;
  }

  @Column(name = "TR_DATETIMEUTC", nullable = false, length = 64)
  public String getTrDatetimeutc() {
    return this.trDatetimeutc;
  }

  public void setTrDatetimeutc(String trDatetimeutc) {
    this.trDatetimeutc = trDatetimeutc;
  }

  @Column(name = "TR_DATETIMELOCAL", nullable = false, length = 64)
  public String getTrDatetimelocal() {
    return this.trDatetimelocal;
  }

  public void setTrDatetimelocal(String trDatetimelocal) {
    this.trDatetimelocal = trDatetimelocal;
  }

  @Column(name = "TR_DIRECTION")
  public Integer getTrDirection() {
    return this.trDirection;
  }

  public void setTrDirection(Integer trDirection) {
    this.trDirection = trDirection;
  }

  @Column(name = "TR_DATA", length = 512)
  public String getTrData() {
    return this.trData;
  }

  public void setTrData(String trData) {
    this.trData = trData;
  }

  @Column(name = "TR_CTRL_NAME", nullable = false, length = 64)
  public String getTrCtrlName() {
    return this.trCtrlName;
  }

  public void setTrCtrlName(String trCtrlName) {
    this.trCtrlName = trCtrlName;
  }

  @Column(name = "TR_SLA", nullable = false, length = 8)
  public String getTrSla() {
    return this.trSla;
  }

  public void setTrSla(String trSla) {
    this.trSla = trSla;
  }

  @Column(name = "TR_DEV_NAME", columnDefinition = "NVARCHAR", length = 64)
  public String getTrDevName() {
    return this.trDevName;
  }

  public void setTrDevName(String trDevName) {
    this.trDevName = trDevName;
  }

  @Column(name = "TR_DEV_FIXEDADDR", nullable = false, length = 64)
  public String getTrDevFixedaddr() {
    return this.trDevFixedaddr;
  }

  public void setTrDevFixedaddr(String trDevFixedaddr) {
    this.trDevFixedaddr = trDevFixedaddr;
  }

  @Column(name = "TR_DEV_SERIAL", nullable = false, length = 64)
  public String getTrDevSerial() {
    return this.trDevSerial;
  }

  public void setTrDevSerial(String trDevSerial) {
    this.trDevSerial = trDevSerial;
  }

  @Column(name = "TR_BUILDING_NAME", length = 64)
  public String getTrBuildingName() {
    return this.trBuildingName;
  }

  public void setTrBuildingName(String trBuildingName) {
    this.trBuildingName = trBuildingName;
  }

  @Column(name = "TR_FLOOR_NAME", length = 64)
  public String getTrFloorName() {
    return this.trFloorName;
  }

  public void setTrFloorName(String trFloorName) {
    this.trFloorName = trFloorName;
  }

  @Column(name = "TR_LOC_NAME", length = 64)
  public String getTrLocName() {
    return this.trLocName;
  }

  public void setTrLocName(String trLocName) {
    this.trLocName = trLocName;
  }

  @Column(name = "TR_ZONE_NAME", length = 64)
  public String getTrZoneName() {
    return this.trZoneName;
  }

  public void setTrZoneName(String trZoneName) {
    this.trZoneName = trZoneName;
  }

  @Column(name = "TR_CZ_NAME", length = 64)
  public String getTrCzName() {
    return this.trCzName;
  }

  public void setTrCzName(String trCzName) {
    this.trCzName = trCzName;
  }

  @Column(name = "TR_DEPT_NAME", length = 64)
  public String getTrDeptName() {
    return this.trDeptName;
  }

  public void setTrDeptName(String trDeptName) {
    this.trDeptName = trDeptName;
  }

  @Column(name = "TR_COMP_NAME", length = 64)
  public String getTrCompName() {
    return this.trCompName;
  }

  public void setTrCompName(String trCompName) {
    this.trCompName = trCompName;
  }

  @Column(name = "TR_TAGCODE", length = 64)
  public String getTrTagcode() {
    return this.trTagcode;
  }

  public void setTrTagcode(String trTagcode) {
    this.trTagcode = trTagcode;
  }

  @Column(name = "TR_TAG_TYPE_ID")
  public Integer getTrTagTypeId() {
    return this.trTagTypeId;
  }

  public void setTrTagTypeId(Integer trTagTypeId) {
    this.trTagTypeId = trTagTypeId;
  }

  @Column(name = "TR_REASONCODE")
  public Integer getTrReasoncode() {
    return this.trReasoncode;
  }

  public void setTrReasoncode(Integer trReasoncode) {
    this.trReasoncode = trReasoncode;
  }

  @Column(name = "TR_PROCESSED", length = 64)
  public String getTrProcessed() {
    return this.trProcessed;
  }

  public void setTrProcessed(String trProcessed) {
    this.trProcessed = trProcessed;
  }

  @Column(name = "ACK_DATETIME", length = 64)
  public String getAckDatetime() {
    return this.ackDatetime;
  }

  public void setAckDatetime(String ackDatetime) {
    this.ackDatetime = ackDatetime;
  }

  @Column(name = "ACK_REPSONSE", length = 1024)
  public String getAckRepsonse() {
    return this.ackRepsonse;
  }

  public void setAckRepsonse(String ackRepsonse) {
    this.ackRepsonse = ackRepsonse;
  }

  @Column(name = "TR_SITE_NAME", length = 64)
  public String getTrSiteName() {
    return this.trSiteName;
  }

  public void setTrSiteName(String trSiteName) {
    this.trSiteName = trSiteName;
  }

  @Column(name = "TR_INJECTED", nullable = false)
  public int getTrInjected() {
    return this.trInjected;
  }

  public void setTrInjected(int trInjected) {
    this.trInjected = trInjected;
  }

  @Column(name = "TR_NESTED_ZONE")
  public Integer getTrNestedZone() {
    return this.trNestedZone;
  }

  public void setTrNestedZone(Integer trNestedZone) {
    this.trNestedZone = trNestedZone;
  }

  @Column(name = "TR_REASON_CODE_NAME", length = 61)
  public String getTrReasonCodeName() {
    return this.trReasonCodeName;
  }

  public void setTrReasonCodeName(String trReasonCodeName) {
    this.trReasonCodeName = trReasonCodeName;
  }

  @Column(name = "TR_TAGCODE_NUM_BITS", nullable = false)
  public int getTrTagcodeNumBits() {
    return this.trTagcodeNumBits;
  }

  public void setTrTagcodeNumBits(int trTagcodeNumBits) {
    this.trTagcodeNumBits = trTagcodeNumBits;
  }

  @Column(name = "TR_EXPIRY_REASON_ID")
  public Integer getTrExpiryReasonId() {
    return this.trExpiryReasonId;
  }

  public void setTrExpiryReasonId(Integer trExpiryReasonId) {
    this.trExpiryReasonId = trExpiryReasonId;
  }

  @Column(name = "TR_EXPIRY_REASON_NAME", length = 64)
  public String getTrExpiryReasonName() {
    return this.trExpiryReasonName;
  }

  public void setTrExpiryReasonName(String trExpiryReasonName) {
    this.trExpiryReasonName = trExpiryReasonName;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "transackByMaFirstAllowedTrnId")
  public Set<MasterAsset> getMasterAssetsForMaFirstAllowedTrnId() {
    return this.masterAssetsForMaFirstAllowedTrnId;
  }

  public void setMasterAssetsForMaFirstAllowedTrnId(Set<MasterAsset> masterAssetsForMaFirstAllowedTrnId) {
    this.masterAssetsForMaFirstAllowedTrnId = masterAssetsForMaFirstAllowedTrnId;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "transackByMaLastAllowedTrnId")
  public Set<MasterAsset> getMasterAssetsForMaLastAllowedTrnId() {
    return this.masterAssetsForMaLastAllowedTrnId;
  }

  public void setMasterAssetsForMaLastAllowedTrnId(Set<MasterAsset> masterAssetsForMaLastAllowedTrnId) {
    this.masterAssetsForMaLastAllowedTrnId = masterAssetsForMaLastAllowedTrnId;
  }

}
