package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * FloorPlan generated by hbm2java
 */
@Entity
@Table(name = "FLOOR_PLAN")
public class FloorPlan implements java.io.Serializable {

  private int floorPlanId;
  private Floor floor;
  private MapElement mapElement;

  public FloorPlan() {}

  public FloorPlan(int floorPlanId, Floor floor, MapElement mapElement) {
    this.floorPlanId = floorPlanId;
    this.floor = floor;
    this.mapElement = mapElement;
  }

  @Id

  @Column(name = "FLOOR_PLAN_ID", unique = true, nullable = false)
  public int getFloorPlanId() {
    return this.floorPlanId;
  }

  public void setFloorPlanId(int floorPlanId) {
    this.floorPlanId = floorPlanId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "FLOOR_ID", nullable = false)
  public Floor getFloor() {
    return this.floor;
  }

  public void setFloor(Floor floor) {
    this.floor = floor;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "MAP_ELEMENT_ID", nullable = false)
  public MapElement getMapElement() {
    return this.mapElement;
  }

  public void setMapElement(MapElement mapElement) {
    this.mapElement = mapElement;
  }

}
