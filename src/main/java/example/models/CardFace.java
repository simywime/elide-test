package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * CardFace generated by hbm2java
 */
@Entity
@Table(name = "CARD_FACE")
public class CardFace implements java.io.Serializable {

  private int cardFaceId;
  private Card card;
  private int cfFront;
  private String cfColour;
  private int cfMarginX;
  private int cfMarginY;
  private Set<CardField> cardFields = new HashSet<CardField>(0);

  public CardFace() {}

  public CardFace(int cardFaceId, Card card, int cfFront, String cfColour, int cfMarginX, int cfMarginY) {
    this.cardFaceId = cardFaceId;
    this.card = card;
    this.cfFront = cfFront;
    this.cfColour = cfColour;
    this.cfMarginX = cfMarginX;
    this.cfMarginY = cfMarginY;
  }

  public CardFace(int cardFaceId, Card card, int cfFront, String cfColour, int cfMarginX, int cfMarginY, Set<CardField> cardFields) {
    this.cardFaceId = cardFaceId;
    this.card = card;
    this.cfFront = cfFront;
    this.cfColour = cfColour;
    this.cfMarginX = cfMarginX;
    this.cfMarginY = cfMarginY;
    this.cardFields = cardFields;
  }

  @Id

  @Column(name = "CARD_FACE_ID", unique = true, nullable = false)
  public int getCardFaceId() {
    return this.cardFaceId;
  }

  public void setCardFaceId(int cardFaceId) {
    this.cardFaceId = cardFaceId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CARD_ID", nullable = false)
  public Card getCard() {
    return this.card;
  }

  public void setCard(Card card) {
    this.card = card;
  }

  @Column(name = "CF_FRONT", nullable = false)
  public int getCfFront() {
    return this.cfFront;
  }

  public void setCfFront(int cfFront) {
    this.cfFront = cfFront;
  }

  @Column(name = "CF_COLOUR", nullable = false, length = 9)
  public String getCfColour() {
    return this.cfColour;
  }

  public void setCfColour(String cfColour) {
    this.cfColour = cfColour;
  }

  @Column(name = "CF_MARGIN_X", nullable = false)
  public int getCfMarginX() {
    return this.cfMarginX;
  }

  public void setCfMarginX(int cfMarginX) {
    this.cfMarginX = cfMarginX;
  }

  @Column(name = "CF_MARGIN_Y", nullable = false)
  public int getCfMarginY() {
    return this.cfMarginY;
  }

  public void setCfMarginY(int cfMarginY) {
    this.cfMarginY = cfMarginY;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "cardFace")
  public Set<CardField> getCardFields() {
    return this.cardFields;
  }

  public void setCardFields(Set<CardField> cardFields) {
    this.cardFields = cardFields;
  }

}
