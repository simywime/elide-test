package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Host generated by hbm2java
 */
@Entity
@Table(name = "HOST", uniqueConstraints = @UniqueConstraint(columnNames = {"HOST_ID", "SITE_ID"}))
public class Host implements java.io.Serializable {

  private int hostId;
  private Site site;
  private String hostMachineaddr;
  private String hostIpaddress;
  private short hostModified;
  private String hiFixedaddr;

  public Host() {}

  public Host(int hostId, Site site, short hostModified) {
    this.hostId = hostId;
    this.site = site;
    this.hostModified = hostModified;
  }

  public Host(int hostId, Site site, String hostMachineaddr, String hostIpaddress, short hostModified, String hiFixedaddr) {
    this.hostId = hostId;
    this.site = site;
    this.hostMachineaddr = hostMachineaddr;
    this.hostIpaddress = hostIpaddress;
    this.hostModified = hostModified;
    this.hiFixedaddr = hiFixedaddr;
  }

  @Id

  @Column(name = "HOST_ID", unique = true, nullable = false)
  public int getHostId() {
    return this.hostId;
  }

  public void setHostId(int hostId) {
    this.hostId = hostId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SITE_ID", nullable = false)
  public Site getSite() {
    return this.site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  @Column(name = "HOST_MACHINEADDR", length = 64)
  public String getHostMachineaddr() {
    return this.hostMachineaddr;
  }

  public void setHostMachineaddr(String hostMachineaddr) {
    this.hostMachineaddr = hostMachineaddr;
  }

  @Column(name = "HOST_IPADDRESS", length = 64)
  public String getHostIpaddress() {
    return this.hostIpaddress;
  }

  public void setHostIpaddress(String hostIpaddress) {
    this.hostIpaddress = hostIpaddress;
  }

  @Column(name = "HOST_MODIFIED", nullable = false)
  public short getHostModified() {
    return this.hostModified;
  }

  public void setHostModified(short hostModified) {
    this.hostModified = hostModified;
  }

  @Column(name = "HI_FIXEDADDR", length = 8)
  public String getHiFixedaddr() {
    return this.hiFixedaddr;
  }

  public void setHiFixedaddr(String hiFixedaddr) {
    this.hiFixedaddr = hiFixedaddr;
  }

}
