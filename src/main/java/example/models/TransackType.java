package example.models;
// Generated Dec 8, 2020 4:36:36 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TransackType generated by hbm2java
 */
@Entity
@Table(name = "TRANSACK_TYPE")
public class TransackType implements java.io.Serializable {

  private int transackTypeId;
  private String transackTypeName;
  private Set<Transack> transacks = new HashSet<Transack>(0);

  public TransackType() {}

  public TransackType(int transackTypeId) {
    this.transackTypeId = transackTypeId;
  }

  public TransackType(int transackTypeId, String transackTypeName, Set<Transack> transacks) {
    this.transackTypeId = transackTypeId;
    this.transackTypeName = transackTypeName;
    this.transacks = transacks;
  }

  @Id

  @Column(name = "TRANSACK_TYPE_ID", unique = true, nullable = false)
  public int getTransackTypeId() {
    return this.transackTypeId;
  }

  public void setTransackTypeId(int transackTypeId) {
    this.transackTypeId = transackTypeId;
  }

  @Column(name = "TRANSACK_TYPE_NAME", length = 64)
  public String getTransackTypeName() {
    return this.transackTypeName;
  }

  public void setTransackTypeName(String transackTypeName) {
    this.transackTypeName = transackTypeName;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "transackType")
  public Set<Transack> getTransacks() {
    return this.transacks;
  }

  public void setTransacks(Set<Transack> transacks) {
    this.transacks = transacks;
  }

}
